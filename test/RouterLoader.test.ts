import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { FatalError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import express from "express";
import { RouterLoader } from "../src/RouterLoader";

chai.use(chaiAsPromised);

describe("RouterLoader", () => {
    describe("loadRouters", () => {
        it("should load routers", async () => {
            const routerLoader = new RouterLoader(["pid"]);

            const result = await routerLoader["loadRouters"]();

            expect(Array.isArray(result)).to.be.true;
            expect(result.length).to.be.greaterThan(0);
            expect(result.filter((router) => router instanceof AbstractRouter)).to.have.length(result.length);
        });

        it("should reject (non-existent module)", async () => {
            const routerLoader = new RouterLoader(["non-existing"]);

            await expect(routerLoader["loadRouters"]()).to.be.rejectedWith(
                FatalError,
                "Cannot import router for module: @golemio/non-existing/dist/output-gateway."
            );
        });
    });

    describe("registerRouters", () => {
        it("should register routers", async () => {
            const app = express();
            const routerLoader = new RouterLoader(["pid"]);

            await routerLoader.registerRouters(app);

            const routes = app._router.stack.filter((r: any) => r.name === "router"); // get all the paths
            expect(routes.length).to.be.greaterThan(0);
        });
    });
});
