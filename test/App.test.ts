import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import sinon from "sinon";
import express from "@golemio/core/dist/shared/express";
import { config } from "@golemio/core/dist/output-gateway/config";
import App from "../src/App";

chai.use(chaiAsPromised);

const DEFAULT_ACCEPT_ENCODING = "gzip";

describe("App", () => {
    let expressApp: express.Application;
    let app: App;
    let sandbox: sinon.SinonSandbox;
    let exitStub: any;

    before(async () => {
        app = new App();
        await app.start();
        expressApp = app.express;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        exitStub = sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should start", async () => {
        expect(expressApp).not.to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
    });

    it("should have health check on /", (done) => {
        request(expressApp).get("/").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should have health check on /health-check", (done) => {
        request(expressApp)
            .get("/health-check")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should return 404 on non-existing route /non-existing", (done) => {
        request(expressApp)
            .get("/non-existing")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404, done);
    });

    describe("shouldCompress", () => {
        let shouldCompressSpy: sinon.SinonSpy;

        beforeEach(() => {
            shouldCompressSpy = sandbox.spy(app as any, "shouldCompress");
        });

        describe("on routes with _shouldCompressByDefault enabled", () => {
            let defaultRequestHandlerStub: sinon.SinonStub;

            beforeEach(() => {
                defaultRequestHandlerStub = sandbox.stub(app as any, "defaultRequestHandler") as sinon.SinonStub;
                defaultRequestHandlerStub.callsFake(
                    async (_req: express.Request, res: express.Response, _next: express.NextFunction) => {
                        res._shouldCompressByDefault = true;
                        return res.json("Hello world!");
                    }
                );
            });

            it("should return true and set a default Accept-Encoding header for requests with no Accept-Encoding", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals(DEFAULT_ACCEPT_ENCODING);
                expect(shouldCompressSpy.firstCall.args[1]).to.be.an("object").with.property("_shouldCompressByDefault").that.is
                    .true;
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });

            it("should return true and keep the same Accept-Encoding header for requests with Accept-Encoding: gzip", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "gzip")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals("gzip");
                expect(shouldCompressSpy.firstCall.args[1]).to.be.an("object").with.property("_shouldCompressByDefault").that.is
                    .true;
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });

            it("should return true and keep the same Accept-Encoding header for requests with Accept-Encoding: identity", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "identity")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals("identity");
                expect(shouldCompressSpy.firstCall.args[1]).to.be.an("object").with.property("_shouldCompressByDefault").that.is
                    .true;
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });
        });

        describe("on routes without _shouldCompressByDefault enabled", () => {
            let defaultRequestHandlerStub: sinon.SinonStub;

            beforeEach(() => {
                defaultRequestHandlerStub = sandbox.stub(app as any, "defaultRequestHandler") as sinon.SinonStub;
                defaultRequestHandlerStub.callsFake(
                    async (_req: express.Request, res: express.Response, _next: express.NextFunction) => {
                        return res.json("Hello world!");
                    }
                );
            });

            it("should return true and keep the same Accept-Encoding header for requests with no Accept-Encoding", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals("");
                expect(shouldCompressSpy.firstCall.args[1])
                    .to.be.an("object")
                    .that.does.not.have.property("_shouldCompressByDefault");
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });

            it("should return true and keep the same Accept-Encoding header for requests with Accept-Encoding: gzip", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "gzip")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals("gzip");
                expect(shouldCompressSpy.firstCall.args[1])
                    .to.be.an("object")
                    .that.does.not.have.property("_shouldCompressByDefault");
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });

            it("should return true and keep the same Accept-Encoding header for requests with Accept-Encoding: identity", async () => {
                const res = await request(expressApp)
                    .get("/")
                    .set("Accept", "application/json")
                    .set("Accept-Encoding", "identity")
                    .send();

                expect(res.status).to.equal(200);
                expect(res.headers["content-type"]).to.be.a("string").that.includes("json");
                expect(defaultRequestHandlerStub.calledOnce).to.be.true;
                expect(shouldCompressSpy.calledOnce).to.be.true;
                expect(shouldCompressSpy.firstCall.args[0]).to.be.an("object").with.property("headers");
                expect(shouldCompressSpy.firstCall.args[0].headers)
                    .to.be.an("object")
                    .with.property("accept-encoding")
                    .that.equals("identity");
                expect(shouldCompressSpy.firstCall.args[1])
                    .to.be.an("object")
                    .that.does.not.have.property("_shouldCompressByDefault");
                expect(shouldCompressSpy.firstCall.returnValue).to.be.true;
            });
        });
    });
});
