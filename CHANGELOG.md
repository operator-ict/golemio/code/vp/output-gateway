# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [4.5.3] - 2025-03-11

### Removed

-   Remove sentry ([core#128](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/128))

## [4.5.2] - 2025-03-03

-   No changelog

## [4.5.1] - 2025-02-24

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [4.5.0] - 2025-01-29

-   No changelog

## [4.4.0] - 2025-01-27

-   No changelog

## [4.3.1] - 2025-01-22

-   No changelog

## [4.3.0] - 2025-01-20

### Changed

-   Optimize CI/CD and build of Docker image ([core#126](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/126))
-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [4.2.0] - 2024-12-17

### Changed

-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))
-   handle Error 406 as warning ([#226](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/226))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [4.1.2] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [4.1.1] - 2024-11-27

### Added

-   Plausible for tracking user behavior in "/docs/openapi" ([golemio#631](https://gitlab.com/operator-ict/golemio/code/general/-/issues/631))

## [4.1.0] - 2024-11-25

-   No changelog

## [4.0.0] - 2024-11-13

### Removed

-   **Breaking:** EP `/v2/departureboards` that was deprecated several years ago and is no longer maintained ([pid#428](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/428))
-   **Breaking:** support for Node.js versions <20.12.0

## [3.12.7] - 2024-11-06

-   No changelog

## [3.12.6] - 2024-10-23

-   No changelog

## [3.12.5] - 2024-10-14

-   No changelog

## [3.12.4] - 2024-09-30

-   No changelog

## [3.12.3] - 2024-09-18

-   No changelog

## [3.12.2] - 2024-09-09

### Changed

-   Replace node-redis with ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [3.12.1] - 2024-09-02

### Added

-   Introduce proper API versioning ([vymi-alerts#30](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/30))
-   Gzip encoding to openapi ([pid#413](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/413))

## [3.12.0] - 2024-08-19

### Removed

-   remove CacheMiddleware ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

## [3.11.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [3.11.0] - 2024-08-14

-   No changelog

## [3.10.0] - 2024-08-07

-   No changelog

## [3.9.0] - 2024-07-31

-   No changelog

## [3.8.1] - 2024-07-29

-   No changelog

## [3.8.0] - 2024-07-17

### Added

-   Optionally apply compression to OG responses by default, unless explicitly requested otherwise ([pid#383](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/383))

### Changed

-   Update Sinon.JS

## [3.7.4] - 2024-07-15

-   No changelog

## [3.7.3] - 2024-07-10

-   No changelog

## [3.7.2] - 2024-07-04

-   No changelog

## [3.7.1] - 2024-07-01

-   No changelog

## [3.7.0] - 2024-06-24

-   No changelog

## [3.6.11] - 2024-06-12

-   No changelog

## [3.6.10] - 2024-06-03

### Added

-   New public GTFS router

## [3.6.9] - 2024-05-29

-   No changelog

## [3.6.8] - 2024-05-20

-   No changelog

## [3.6.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [3.6.6] - 2024-05-09

-   No changelog

## [3.6.5] - 2024-05-06

### Added

-   New public departures EP

## [3.6.4] - 2024-04-29

-   No changelog

## [3.6.3] - 2024-04-24

-   No changelog

## [3.6.2] - 2024-04-15

-   No changelog

## [3.6.1] - 2024-04-15

### Added

-   Expose static openapi docs files

## [3.6.0] - 2024-04-10

-   No changelog

## [3.5.10] - 2024-03-25

-   No changelog

## [3.5.9] - 2024-02-21

-   No changelog

## [3.5.8] - 2024-02-19

-   No changelog

## [3.5.7] - 2024-02-14

-   No changelog

## [3.5.6] - 2024-02-12

-   No changelog

## [3.5.5] - 2024-02-05

-   No changelog

## [3.5.4] - 2024-01-22

-   No changelog

## [3.5.3] - 2024-01-17

### Changed

-   Improve README.md
-   Update Swagger UI, improve OpenAPI docs

## [3.5.2] - 2024-01-10

-   No changelog

## [3.5.1] - 2023-12-20

### Fixed

-   Improve request aborted handling ([core#86](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/86))

## [3.5.0] - 2023-12-18

-   No changelog

## [3.4.10] - 2023-11-15

-   No changelog

## [3.4.9] - 2023-11-13

### Added

-   Public API for vehicle positions

## [3.4.8] - 2023-10-23

-   No changelog

## [3.4.7] - 2023-10-09

### Changed

-   CI/CD - Update Redis services to v7.2.1
-   Update openapi docs, change swagger-ui config

## [3.4.6] - 2023-10-02

-   No changelog

## [3.4.5] - 2023-09-11

-   No changelog

## [3.4.4] - 2023-09-04

-   No changelog

## [3.4.3] - 2023-08-23

### Changed

-   Generate OAS file via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [3.4.2] - 2023-08-21

No changelog

## [3.4.1] - 2023-08-07

No changelog

## [3.4.0] - 2023-08-02

No changelog

## [3.3.9] - 2023-07-31

No changelog

## [3.3.8] - 2023-07-26

No changelog

## [3.3.7] - 2023-07-24

No changelog

## [3.3.6] - 2023-07-17

### Fixed

-   Open telemetry initialization

## [3.3.5] - 2023-06-26

No changelog

## [3.3.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))
-   Refactoring connectors ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [3.3.3] - 2023-06-05

No changelog

## [3.3.2] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [3.3.1] - 2023-05-25

No changelog

## [3.3.0] - 2023-05-22

No changelog

## [3.2.9] - 2023-05-17

No changelog

## [3.2.8] - 2023-05-10

No changelog

## [3.2.7] - 2023-04-26

No changelog

## [3.2.6] - 2023-04-19

No changelog

## [3.2.5] - 2023-04-12

No changelog

## [3.2.4] - 2023-04-05

No changelog

## [3.2.3] - 2023-03-29

No changelog

## [3.2.2] - 2023-03-27

No changelog

## [3.2.1] - 2023-03-15

No changelog

## [3.2.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

### Fixed

-   Update openapi docs header

## [3.1.2] - 2023-02-15

No changelog

## [3.1.1] - 2023-01-30

### Added

-   Feature flag service connection

## [3.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [3.0.44] - 2023-01-04

### Changed

-   Fixed markdown links, removed section Versions

## [3.0.43] - 2022-12-13

No changelog

## [3.0.42] - 2022-11-29

### Changed

-   Stop serving `/.well-known/security.txt`

### Removed

-   Remove Express serve-static middleware ([vp/og#1](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/-/issues/1))

## [3.0.41] - 2022-11-10

No changelog

## [3.0.40] - 2022-10-13

### Changed

-   Update TypeScript to v4.7.2

### Removed

-   Remove dredd tests ([pid#179](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/179))

## [3.0.39] - 2022-10-05

No changelog

## [3.0.38] - 2022-10-04

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))

