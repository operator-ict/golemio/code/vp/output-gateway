[![pipeline status](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/commits/master)

# Golemio VP Output Gateway

> Fork of [Output Gateway](https://gitlab.com/operator-ict/golemio/code/output-gateway) dedicated only to Public Transport data.

The Output Gateway is an integral part of the Golemio data platform system, serving as a unified REST API for accessing data stored within the Golemio data platform.

This implementation utilizes the Express server and incorporates caching mechanisms powered by Redis.

For comprehensive documentation, please visit: [Golemio Output Gateway Documentation](https://operator-ict.gitlab.io/golemio/code/output-gateway/)

Refer to the API documentation at:

-   [Main (production) Public Transport API Documentation](https://api.golemio.cz/pid/docs/openapi/)
-   [Test (development) Public Transport API Documentation](https://rabin.golemio.cz/pid/docs/openapi/)

Developed by http://operatorict.cz
