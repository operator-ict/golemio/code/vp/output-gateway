import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { Router } from "express";

type RouterLoaderOutput = AbstractRouter[];

export class RouterLoader {
    constructor(private readonly modules: string[]) {}

    private async loadRouters(): Promise<RouterLoaderOutput> {
        let routerList: RouterLoaderOutput = [];

        for (const module of this.modules) {
            const importPath = `@golemio/${module}/dist/output-gateway`;

            try {
                const { routers } = await import(importPath);
                if (!this.isAbstractRouterArray(routers)) {
                    throw new FatalError(`Incorrect router export.`, this.constructor.name);
                }
                routerList = routerList.concat(...routers);
            } catch (err) {
                throw new FatalError(`Cannot import router for module: ${importPath}.`, this.constructor.name, err);
            }
        }

        return routerList;
    }

    private isAbstractRouterArray(element: unknown): element is RouterLoaderOutput {
        return Array.isArray(element) && element.every((r: unknown) => r instanceof AbstractRouter);
    }

    public async registerRouters(appRouter: Router) {
        const routerList = await this.loadRouters();

        for (const moduleRouter of routerList) {
            appRouter.use(moduleRouter.getPath(), moduleRouter.getRouter());
        }
    }
}
